from django.contrib import admin
from books.models import Book, Magazine, Genre, Issue

admin.site.register(Book)
admin.site.register(Magazine)
admin.site.register(Genre)
admin.site.register(Issue)
