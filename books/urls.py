from django.urls import path
from books.views import(
    delete_book,
    show_books,
    create_book,
    show_book,
    delete_book,
    update_book
)
# we need to import the views file to reference the functions defined in views.py

urlpatterns = [
    # Call function "book_list()" to get the html render
    # This is path /books/
    path('', show_books, name="list_books"),
    path('create/', create_book, name="create_book"),
    path('<int:pk>/', show_book, name="book_detail"),
    path('<int:pk>/delete', delete_book, name="delete_book"),
    path('<int:pk>/update', update_book, name="update_book"),
]

# we need to register books URLs inside the main URLs file